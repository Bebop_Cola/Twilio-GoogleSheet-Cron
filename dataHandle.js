const google = require('googleapis');
const configs = require('./configs');
const client = require('twilio')(configs.accountSid, configs.authToken);
const authentication = require('./authentication');
const CronJob = require('cron').CronJob;

const dh = {};

dh.cronAppointment = [];
dh.activeTasks = {};
dh.Tasks = {};
dh.currentAppointment = [];

dh.getData = (auth, range, id) => new Promise((resolve, reject) => {
  const sheets = google.sheets('v4');
  sheets.spreadsheets.values.get({
    auth,
    spreadsheetId: id,
    range,
  }, (err, response) => {
    if (err) {
      reject();
    }
    const rows = response.values;
    if (rows.length === 0) {
      reject();
    } else {
      const rowArr = [];
      for (let i = 0; i < rows.length; i += 1) {
        rowArr.push(rows[i]);
      }
      resolve(rowArr);
    }
  });
});

// arrVal is an array of item values, [column1, column2, etc...]
dh.appendData = (auth, range, id, arrVal) => new Promise((resolve, reject) => {
  const sheets = google.sheets('v4');
  sheets.spreadsheets.values.append({
    auth,
    spreadsheetId: id,
    range,
    valueInputOption: 'USER_ENTERED',
    resource: {
      range,
      values: [
        arrVal,
      ],
    },
  }, (err) => {
    if (err) {
      console.log(`The API returned an error: ${err}`);
      reject();
    } else {
      console.log('Appended');
      resolve();
    }
  });
});

dh.updateData = (auth, range, id, arrVal) => new Promise((resolve, reject) => {
  const sheets = google.sheets('v4');
  sheets.spreadsheets.values.update({
    auth,
    spreadsheetId: id,
    range,
    valueInputOption: 'USER_ENTERED',
    resource: {
      range,
      values: [
        arrVal,
      ],
    },
  }, (err) => {
    if (err) {
      console.log(`The API returned an error: ${err}`);
      reject();
    } else {
      console.log('Updated');
      resolve();
    }
  });
});

// Converts English, days and date formats to Cron Format
function dayToCrone(day) {
  const daysObj = configs.days;
  const dayRange = [];
  let cronDay = day.toLowerCase();

  if (cronDay.includes('-')) {
    cronDay = cronDay.split('-');
    dayRange.push(daysObj[cronDay[0]]);
    dayRange.push(daysObj[cronDay[1]]);
    return dayRange.join('-');
  }
  const days = cronDay.split(' ');
  for (let i = 0; i < days.length; i += 1) {
    dayRange.push(daysObj[days[i]]);
  }
  return dayRange.join(',');
}
// Converts ClockTime to Cron
function timeToCrone(Time, day) {
  const cronDay = dayToCrone(day); // temp
  const time = Time.split(':');
  return `${time[1]} ${time[0]} * * ${cronDay}`;
}

dh.addCrone = (text, number, Time, day) => {
  const time = timeToCrone(Time, day);
  return new CronJob(time, () => {
    client.messages.create({
      to: number,
      from: configs.myNumber,
      body: text,
    }, (err, message) => {
      if (err) console.log(err);
      else console.log(message.sid);
    });
  }, null, true, 'America/Los_Angeles');
};

dh.apointmentSender = () => {
  authentication.authenticate().then((auth) => {
    dh.getData(auth, configs.appointments, configs.spreadsheetId).then((result) => {
      for (let i = 0; i < result.length; i += 1) {
        if (dh.currentAppointment.length === 0) {
          const weekOf = result[i][0];
          const dayOf = result[i][1];
          const number = result[i][2];
          const statusOf = result[i][3];
          const message = result[i][4];
          const timeOf = result[i][5];
          const sendMessage = `Please confirm for ${dayOf}, for the week of ${weekOf}`;

          const date = new Date();
          const dateNow = `${(date.getMonth() + 1).toString()}/${date.getDate().toString()}`;
          const dateRange = weekOf.split('-');
          if (dateNow > dateRange[0] && dateNow < dateRange[1]) {
            dh.currentAppointment.push(weekOf, dayOf, number, statusOf, message, timeOf, sendMessage);
            if (dh.currentAppointment[3] === 'unconfirmed') {
              dh.cronAppointment = dh.addCrone(sendMessage, number, '18:00', 'm-f');
            }
          }
        }
      }
    });
  });
};

dh.startCron = () => {
  authentication.authenticate().then((auth) => {
    dh.getData(auth, configs.remind, configs.spreadsheetId).then((result) => {
      for (let i = 0; i < result.length; i += 1) {
        dh.Tasks[result[i][0]] = true;
        if (!(result[i][0] in dh.activeTasks)) {
          dh.activeTasks[result[i][0]] = dh.addCrone(result[i][0], result[i][3], result[i][1], result[i][2]);
        }
      }
      const activeArray = Object.keys(dh.activeTasks);
      for (let i = 0; i < activeArray.length; i += 1) {
        if (!(activeArray[i] in dh.Tasks)) {
          dh.activeTasks[activeArray[i]].stop();
          delete dh.activeTasks[activeArray[i]];
        }
      }
    });
  });
};

module.exports = dh;
