const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

const authentication = require('./authentication');
const configs = require('./configs');
const dh = require('./dataHandle');

const client = require('twilio')(configs.accountSid, configs.authToken);

const app = express();
app.use(bodyParser.urlencoded({
  extended: true,
}));


// global funcs
function dateConvert(x) {
  const a = new Date();
  const b = new Date(x);
  a.setDate(b.getDate());
  return a.toDateString();
}
const nextDay = (date, Day) => {
  const day = 7 - Day;
  return new Date(+date + (7 - (date.getDay() + day) % 7) * 86400000);
};

app.post('/sms', (req, res) => {
  const twiml = new MessagingResponse();
  const msg = req.body.Body.toLowerCase();
  if (msg.indexOf('shop') === 0) {
    const shop = msg.slice(5);
    let item;
    let num;
    // SHOPPING LIST ADD
    if (shop.includes('add') || shop.includes('+')) {
      if (shop.match(/\d+/)) {
        num = shop.match(/\d+/)[0];
      } else {
        num = 1;
      }
      item = shop.replace(/add|\+|\s|\d+/gi, '');
      authentication.authenticate().then((auth) => {
        dh.appendData(auth, configs.groceries, configs.spreadsheetId, [item, num]);
      });
    } else if (shop.includes('remove') || shop.includes('delete')) {
    // SHOPPING LIST CLEAR
    } else if (shop.includes('clear')) {
    // SHOPPING LIST VIEW
    } else if (shop.includes('view')) {
      authentication.authenticate().then((auth) => {
        dh.getData(auth, configs.groceries, configs.spreadsheetId).then((result) => {
          twiml.message(result.join('\n').toString());
          res.writeHead(200, {
            'Content-Type': 'text/xml',
          });
          res.end(twiml.toString());
        });
      });
    } else {
      twiml.message('No Valid Commands given');
      res.writeHead(200, {
        'Content-Type': 'text/xml',
      });
      res.end(twiml.toString());
    }
  } else if (msg.indexOf('remind') === 0) {
    // "remind add some taks to do @ 10am" --Optional mon-fri, tue etc..
    let remind = msg.slice(7);
    let remindDay;
    let remindMsg;
    let remindTime;
    if (remind.includes('add')) {
      remind = remind.slice(4).split(' @ ');
      remindMsg = remind[0];
      if (remind[1].substring(0, remind[1].indexOf(' ')).length < 1) {
        remindTime = remind[1];
      } else {
        remindTime = remind[1].substring(0, remind[1].indexOf(' '));
      }
      if (remindTime !== remind[1].substring(remind[1].indexOf(' ') + 1)) {
        remindDay = remind[1].substring(remind[1].indexOf(' ') + 1);
      } else {
        remindDay = '0-6';
      }
      authentication.authenticate().then((auth) => {
        dh.appendData(auth,
            configs.remind,
            configs.spreadsheetId,
            [remindMsg, remindTime, remindDay, req.body.From.toString()]);
      });
    } else if (remind.includes('view')) {
      authentication.authenticate().then((auth) => {
        dh.getData(auth, configs.remind, configs.spreadsheetId).then((result) => {
          twiml.message(result.join(' -- '));
          res.writeHead(200, { 'Content-Type': 'text/xml' });
          res.end(twiml.toString());
        });
      });
    } else {
      twiml.message('No Valid Commands given');
      res.writeHead(200, { 'Content-Type': 'text/xml' });
      res.end(twiml.toString());
    }
  } else if (msg.indexOf('confirm') === 0) {
    let date;
    let sliceMsg;
    let finalMsg;
    if (msg.length < 8) {
      date = dh.currentAppointment[1];
    } else {
      sliceMsg = msg.slice(8);
      if (sliceMsg.includes('-')) {
        date = dateConvert(sliceMsg.split('-').join('-'));
      } else if (sliceMsg.includes('/')) {
        date = dateConvert(sliceMsg.split('/').join('-'));
      } else if (sliceMsg.includes('sat')) {
        date = nextDay(new Date(), 6);
      } else if (sliceMsg.includes('sun')) {
        date = nextDay(new Date(), 7);
      } else if (sliceMsg.includes('fri')) {
        date = nextDay(new Date(), 5);
      } else if (sliceMsg.includes('thu')) {
        date = nextDay(new Date(), 4);
      } else {
        date = false;
      }
    }
    if (date === false) {
      finalMsg = 'No Valid Date given';
    } else {
      const stringDate = date.toDateString();
      dh.currentAppointment[1] = `${(date.getMonth() + 1).toString()}/${date.getDate().toString()}`;
      dh.currentAppointment[3] = 'confirmed';
      finalMsg = `Date Confirmed see you at ${stringDate}`;
    }
    twiml.message(finalMsg);
    res.writeHead(200, {
      'Content-Type': 'text/xml',
    });
    res.end(twiml.toString());
    authentication.authenticate().then((auth) => {
      dh.updateData(auth, configs.appointments, configs.spreadsheetId, dh.currentAppointment);
      dh.cronAppointment.stop();
    });
  } else if (msg.indexOf('cancel') === 0) {
    dh.currentAppointment[3] = 'canceled';
    dh.currentAppointment[4] = msg.slice(7);
    twiml.message('Pretty lame tbqh fam');
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    res.end(twiml.toString());
    authentication.authenticate().then((auth) => { dh.updateData(auth, configs.appointments, configs.spreadsheetId, dh.currentAppointment); });
    dh.cronAppointment.stop();
  } else {
    twiml.message('No valid command given');
    res.writeHead(200, {
      'Content-Type': 'text/xml',
    });
    res.end(twiml.toString());
  }
});


http.createServer(app).listen(7070, () => {
  console.log('Express server listening on port 7070');
});

dh.startCron();
dh.apointmentSender();
